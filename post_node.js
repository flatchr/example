const https = require('https');

const fields = {
  token: '8mdGHihkSGTv9JKN3JuS0MTnN2aDjjpbN54Jw5yqBpM=',
  vacancy: 'vyja39jqrrnlnqwe-technicien-metrologue-h-f',
  firstname: 'john',
  lastname: 'doe',
  email: 'webmaster@flatchr.io',
  comment: 'lettre test',
  offerer_id: 75,
  type: 'document',
  resume: {
    data: 'SGVsbG8h=',
    fileName: 'resume.txt',
    contentType: 'application/octet-stream'
  },
  urls: [
    'https://www.linkedin.com/in/martin-de-la-taille-a32b9436/',
    'https://flatchr.io'
  ]
}

const postData = JSON.stringify(fields);

const options = {
  hostname: 'careers.flatchr.io',
  path: '/vacancy/candidate/test',
  method: 'POST',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Content-Length': postData.length
  }
}

const req = https.request(options, (res) => {
  console.log(`statusCode: ${res.statusCode}`)
  var json = '';

  res.on('data', (chunk) => {
    json += chunk;
  })

  res.on('end', () => {
    if (res.statusCode === 200) {
      try {
        var data = JSON.parse(json);
        console.log(data);
      } catch (e) {
        console.log('Error parsing JSON!');
      }
    }
});
})

req.on('error', (error) => {
  console.error(error)
})

req.write(postData)
req.end()