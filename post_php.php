<?php

$url = 'https://careers.flatchr.io/vacancy/candidate/test';

$fields = array(
		'token'		=> '8mdGHihkSGTv9JKN3JuS0MTnN2aDjjpbN54Jw5yqBpM=',
		'vacancy'		=> 'vyja39jqrrnlnqwe-technicien-metrologue-h-f',
		'firstname'		=> 'john',
		'lastname'		=> 'doe',
		'email'				=> 'webmaster@flatchr.io',
		'comment'			=> 'lettre test',
		'offerer_id'	=> 75,
		'type'				=> 'document',
		'resume'      => json_encode(array (
			'fileName' 	=> 'resume.txt',
			'data' 			=> 'SGVsbG8h='
		)),
		'urls'				=> json_encode(array (
			'https://www.linkedin.com/in/martin-de-la-taille-a32b9436/',
			'https://flatchr.io'
		))
	);

	//url-ify the data for the POST
	$fields_string = http_build_query($fields);

	//open connection
	$ch = curl_init();
	$headers = array("Content-Type" => "application/json");
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	var_dump($url);
	//execute post
	$result = curl_exec($ch);
	echo $result;

?>
